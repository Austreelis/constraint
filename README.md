# Constraint

This crate provides a macro to easily declare constrained types, when you need
more guarantees than what an already existing type can provide.
Such types allow to leverage the type system to avoid inconvenient validation
and error handling.

One of the most common example is the non-empty string. Sometimes you need to
assert a string is not empty. Without encoding this property in a type, you
have to check the string isn't empty every time you use it, or rely on a
previous check. This leads to error-prone and more difficult to maintain code.

Creating non-empty string types with this crate can be done easily:
```rust
constraint::constraint!{
    /// A non empty owned string
    pub NonEmptyString(String) if |s: &String| !s.is_empty()
}
```

# License

This project is licensed under the Apache-2.0 or MIT license, at your option.
